#include "main.h"

SAMPFUNCS *SF = new SAMPFUNCS();

bool flag = false;


bool __stdcall raknet_hook(stRakNetHookParams* params) {

	if (flag && params->packetId == RPC_ScrSetPlayerArmour) // possible with health
		return false;
	
	return true;
}

void __stdcall fake_armor(std::string) {
	
	flag = !flag;

	if (flag)
		PEDSELF->SetArmor(100); // possible with health
}

void __stdcall mainloop() {

	static bool initialized = false;

	if (!initialized) {

		if (GAME && GAME->GetSystemState() == eSystemState::GS_PLAYING_GAME && SF->getSAMP()->IsInitialized()) {

			initialized = true;

			SF->getRakNet()->registerRakNetCallback(RakNetScriptHookType::RAKHOOK_TYPE_INCOMING_RPC, raknet_hook);
			SF->getSAMP()->registerChatCommand("fakearmor", fake_armor);
		}
	}
}

int __stdcall DllMain(HMODULE hModule, DWORD dwReasonForCall, LPVOID lpReserved) {

	if (dwReasonForCall == DLL_PROCESS_ATTACH)
		SF->initPlugin(mainloop, hModule);

	return true;
}
